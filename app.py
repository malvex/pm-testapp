from flask import Flask
import redis
import socket
import os

hostname = socket.gethostname()

# reads HOST from REDIS_HOST env variable. Defaults to "redis" if not set.
REDIS_HOST = os.getenv('REDIS_HOST', "redis")

app = Flask(__name__)
redis = redis.Redis(REDIS_HOST)

@app.route("/")
def root():
    redis.incr("visited")
    c = int(redis.get("visited"))
    return f"hello! My hostname is {hostname} and this site was visited {c} times!"


@app.route("/healthz")
def healthz():
    """This only checks if the application is alive, it doesn't check redis connection"""
    return "OK"


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
